package pl.edu.agh.crrobdsice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@SpringBootApplication(exclude = CassandraDataAutoConfiguration.class)
public class CrrobdsiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrrobdsiceApplication.class, args);
	}
}
