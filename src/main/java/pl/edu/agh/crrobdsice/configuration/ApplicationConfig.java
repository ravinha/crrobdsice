package pl.edu.agh.crrobdsice.configuration;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Objects;

@Component
public class ApplicationConfig {

    private Long messageId;

    @PostConstruct
    public void init() {
        Long foundMsgId = getLastMessageId();
        if (Objects.nonNull(foundMsgId)) {
            messageId = foundMsgId;
        } else {
            messageId = 1L;
        }
    }

    private Long getLastMessageId() {
        try {
            return Files.lines(FileSystems.getDefault().getPath("curr_messageid"), StandardCharsets.UTF_8).findFirst().map(Long::valueOf).orElse(null);
        } catch (IOException e) {
            return null;
        }
    }

    public synchronized long getNextMessageId() {
        return ++messageId;
    }

    public synchronized void setNextMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public synchronized long getCurrentMessageId() {
        return messageId;
    }
}
