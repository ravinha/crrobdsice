package pl.edu.agh.crrobdsice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.crrobdsice.configuration.ApplicationConfig;
import pl.edu.agh.crrobdsice.entities.cassandra.Comment;
import pl.edu.agh.crrobdsice.entities.cassandra.CommentKey;
import pl.edu.agh.crrobdsice.entities.cassandra.Post;
import pl.edu.agh.crrobdsice.repositories.cassandra.CassandraCommentRepository;
import pl.edu.agh.crrobdsice.repositories.cassandra.CassandraNonReactivePostRepository;
import pl.edu.agh.crrobdsice.repositories.cassandra.CassandraPostRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static pl.edu.agh.crrobdsice.utils.RestEnum.*;
import static pl.edu.agh.crrobdsice.utils.StatsLogger.*;

@RestController
@RequestMapping("/cassandra/posts")
public class CassandraPostsController {

    @Autowired
    private CassandraPostRepository cassandraPostRepository;

    @Autowired
    private CassandraNonReactivePostRepository cassandraNonReactivePostRepository;

    @Autowired
    private CassandraCommentRepository cassandraCommentRepository;

    @Autowired
    private ApplicationConfig applicationConfig;

    @GetMapping
    public Flux<Post> index() {
        return cassandraPostRepository.findAll();
    }

    @PostMapping
    public Mono<Post> add(@RequestParam("userId") Long userId, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return cassandraPostRepository.insert(new Post(applicationConfig.getNextMessageId(), userId, content)).doOnSuccess(e ->
                logCaPost(CA_POST_ADD, current, e)
        );
    }

    @PostMapping("/nr")
    public Post addNonReactive(@RequestParam("userId") Long userId, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        Post post = cassandraNonReactivePostRepository.insert(new Post(applicationConfig.getNextMessageId(), userId, content));
        logCaPostNonReactive(CA_POST_ADD, current, post);
        return post;
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return cassandraPostRepository.deleteById(id).doOnSuccess(e ->
                logCaPost(CA_POST_DEL, current, id)
        );
    }

    @GetMapping("/{id}")
    public Mono<Post> getById(@PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return cassandraPostRepository.findPostById(id).doOnSuccess(e ->
                logCaPost(CA_POST_GET, current, e)
        );
    }

    @GetMapping("/nr/{id}")
    public Post getByIdNonReactive(@PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        Post post = cassandraNonReactivePostRepository.findPostById(id);
        logCaPostNonReactive(CA_POST_GET, current, post);
        return post;
    }

    @PutMapping("/{id}")
    public Mono<Post> edit(@PathVariable("id") Long id, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return cassandraPostRepository.editContent(id, content).doOnSuccess(e ->
                logCaPost(CA_POST_EDI, current, e)
        );
    }

    @PutMapping("/nr/{id}")
    public Post editNonReactive(@PathVariable("id") Long id, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        Post post = cassandraNonReactivePostRepository.editContent(id, content);
        logCaPostNonReactive(CA_POST_EDI, current, post);
        return post;
    }

    @PostMapping("/{id}/tag")
    public Mono<Post> tag(@PathVariable("id") Long id, @RequestParam("tag") String tag) {
        Long current = System.currentTimeMillis();
        return cassandraPostRepository.tag(id, Collections.singletonList(tag)).doOnSuccess(e ->
                logCaPost(CA_POST_TAG, current, e)
        );
    }

    @GetMapping("/{postId}/comments")
    public Flux<Comment> getComments(@PathVariable("postId") Long postId) {
        Long current = System.currentTimeMillis();
        return cassandraCommentRepository.findAllByCommentKeyPostId(postId).doOnNext(e ->
                logCaComment(CA_COMM_GET_ALL, current, e)
        );
    }

    @PostMapping("/{postId}/comments")
    public Mono<Comment> addComment(@PathVariable("postId") Long postId, @RequestParam("content") String content, @RequestParam("userId") Long userId) {
        Long current = System.currentTimeMillis();
        return cassandraCommentRepository.insert(new Comment(new CommentKey(postId, applicationConfig.getNextMessageId()), userId, content)).doOnSuccess(e ->
                logCaComment(CA_COMM_ADD, current, e)
        );
    }

    @GetMapping("/{postId}/comments/{id}")
    public Mono<Comment> getCommentsById(@PathVariable("postId") Long postId, @PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return cassandraCommentRepository.findDistinctByCommentKeyPostIdAndCommentKeyCommentId(postId, id).doOnSuccess(e ->
                logCaComment(CA_COMM_GET_BY_ID, current, e)
        );
    }

    @PutMapping("/{postId}/comments/{id}")
    public Mono<Comment> editCommentsById(@PathVariable("postId") Long postId, @PathVariable("id") Long id, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return cassandraCommentRepository.editByCommentKeyPostIdAndCommentKeyCommentId(postId, id, content).doOnSuccess(e ->
                logCaComment(CA_COMM_EDI, current, e)
        );
    }

    @DeleteMapping("/{postId}/comments/{id}")
    public Mono<Void> deleteCommentById(@PathVariable("postId") Long postId, @PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return cassandraCommentRepository.deleteById(new CommentKey(postId, id)).doOnSuccess(e ->
                logCaComment(CA_COMM_DEL, current, id, postId)
        );
    }

}
