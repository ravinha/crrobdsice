package pl.edu.agh.crrobdsice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.crrobdsice.configuration.ApplicationConfig;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/configuration")
public class ConfigurationController {

    @Autowired
    private ApplicationConfig applicationConfig;

    @PutMapping("/postId/{id}")
    public Mono<Void> edit(@PathVariable("id") Long id) {
        applicationConfig.setNextMessageId(id);
        return Mono.empty();
    }
}
