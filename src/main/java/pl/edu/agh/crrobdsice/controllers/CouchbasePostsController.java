package pl.edu.agh.crrobdsice.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.crrobdsice.configuration.ApplicationConfig;
import pl.edu.agh.crrobdsice.entities.couchbase.Comment;
import pl.edu.agh.crrobdsice.entities.couchbase.Post;
import pl.edu.agh.crrobdsice.repositories.couchbase.CouchbaseCommentRepository;
import pl.edu.agh.crrobdsice.repositories.couchbase.CouchbasePostRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static pl.edu.agh.crrobdsice.utils.RestEnum.*;
import static pl.edu.agh.crrobdsice.utils.StatsLogger.logCbComment;
import static pl.edu.agh.crrobdsice.utils.StatsLogger.logCbPost;

@RestController
@RequestMapping("/couchbase/posts")
public class CouchbasePostsController {

    @Autowired
    private CouchbasePostRepository couchbasePostRepository;

    @Autowired
    private CouchbaseCommentRepository couchbaseCommentRepository;

    @Autowired
    private ApplicationConfig applicationConfig;

    @GetMapping
    public Flux<Post> index() {
        return couchbasePostRepository.findAll();
    }

    @PostMapping
    public Mono<Post> add(@RequestParam("userId") Long userId, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return couchbasePostRepository.save(new Post(Long.valueOf(applicationConfig.getNextMessageId()).toString(), userId, content)).doOnSuccess(e ->
                logCbPost(CB_POST_ADD, current, e)
        );
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable("id") String id) {
        Long current = System.currentTimeMillis();
        return couchbasePostRepository.deleteById(id).doOnSuccess(e ->
                logCbPost(CB_POST_DEL, current, Long.valueOf(id))
        );
    }

    @GetMapping("/{id}")
    public Mono<Post> getById(@PathVariable("id") String id) {
        Long current = System.currentTimeMillis();
        return couchbasePostRepository.findById(id).doOnSuccess(e ->
                logCbPost(CB_POST_GET, current, e)
        );
    }

    @PutMapping("/{id}")
    public rx.Observable<Post> edit(@PathVariable("id") Long id, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return couchbasePostRepository.getCouchbaseOperations().update(new Post(id.toString(), content)).doOnCompleted(() ->
                logCbPost(CB_POST_EDI, current, id)
        );
    }

    @PostMapping("/{id}/tag")
    public Mono<Post> tag(@PathVariable("id") Long id, @RequestParam("tag") String tag) {
        Long current = System.currentTimeMillis();
        return couchbasePostRepository.tag(id, Collections.singleton(tag)).doOnSuccess(e ->
                logCbPost(CB_POST_TAG, current, e)
        );
    }

    @GetMapping("/{postId}/comments")
    public Flux<Comment> getComments(@PathVariable("postId") Long postId) {
        Long current = System.currentTimeMillis();
        return couchbaseCommentRepository.findAllByPostId(postId).doOnNext(e ->
                logCbComment(CB_COMM_GET_ALL, current, e)
        );
    }

    @PostMapping("/{postId}/comments")
    public Mono<Comment> addComment(@PathVariable("postId") Long postId, @RequestParam("content") String content, @RequestParam("userId") Long userId) {
        Long current = System.currentTimeMillis();
        return couchbaseCommentRepository.save(new Comment(postId, applicationConfig.getNextMessageId(), userId, content)).doOnSuccess(e ->
                logCbComment(CB_COMM_ADD, current, e)
        );
    }

    @GetMapping("/{postId}/comments/{id}")
    public Mono<Comment> getCommentsById(@PathVariable("postId") Long postId, @PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return couchbaseCommentRepository.findDistinctByPostIdAndCommentId(postId, id).doOnSuccess(e ->
                logCbComment(CB_COMM_GET_BY_ID, current, e)
        );
    }

    @PutMapping("/{postId}/comments/{id}")
    public Mono<Comment> editCommentsById(@PathVariable("postId") Long postId, @PathVariable("id") Long id, @RequestParam("content") String content) {
        Long current = System.currentTimeMillis();
        return couchbaseCommentRepository.editByPostIdAndCommentId(postId, id, content).doOnSuccess(e ->
                logCbComment(CB_COMM_EDI, current, e)
        );
    }

    @DeleteMapping("/{postId}/comments/{id}")
    public Mono<Void> deleteCommentById(@PathVariable("postId") Long postId, @PathVariable("id") Long id) {
        Long current = System.currentTimeMillis();
        return couchbaseCommentRepository.deleteById(id).doOnSuccess(e ->
                logCbComment(CB_COMM_DEL, current, id, postId)
        );
    }

}