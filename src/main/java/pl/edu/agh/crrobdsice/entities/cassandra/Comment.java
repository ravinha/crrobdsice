package pl.edu.agh.crrobdsice.entities.cassandra;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table("comments")
public class Comment implements Serializable {

    @PrimaryKey
    private CommentKey commentKey;

    @Column("owner_id")
    private Long ownerId;

    @Column("content")
    private String content;

    @Column("creation_date")
    private Date creationDate;

    @Column("edition_date")
    private Date editionDate;

    @Column("likes")
    private BigDecimal likes;

    public Comment() {
    }

    public Comment(CommentKey commentKey, Long ownerId, String content) {
        this.commentKey = commentKey;
        this.ownerId = ownerId;
        this.content = content;
        this.creationDate = new Date();
        this.editionDate = new Date();
        this.likes = BigDecimal.ZERO;
    }

    public CommentKey getCommentKey() {
        return commentKey;
    }

    public void setCommentKey(CommentKey commentKey) {
        this.commentKey = commentKey;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(Date editionDate) {
        this.editionDate = editionDate;
    }

    public BigDecimal getLikes() {
        return likes;
    }

    public void setLikes(BigDecimal likes) {
        this.likes = likes;
    }
}
