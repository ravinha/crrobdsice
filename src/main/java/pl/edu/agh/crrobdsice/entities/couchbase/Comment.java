package pl.edu.agh.crrobdsice.entities.couchbase;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Document
public class Comment implements Serializable {

    @Id
    private Long commentId;

    @Field
    private Long postId;

    @Field
    private Long ownerId;

    @Field
    private String content;

    @Field
    private Date creationDate;

    @Field
    private Date editionDate;

    @Field
    private BigDecimal likes;

    public Comment() {
    }

    public Comment(Long commentId, Long postId, Long ownerId, String content) {
        this.commentId = commentId;
        this.postId = postId;
        this.ownerId = ownerId;
        this.content = content;
        this.creationDate = new Date();
        this.editionDate = new Date();
        this.likes = BigDecimal.ZERO;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(Date editionDate) {
        this.editionDate = editionDate;
    }

    public BigDecimal getLikes() {
        return likes;
    }

    public void setLikes(BigDecimal likes) {
        this.likes = likes;
    }
}
