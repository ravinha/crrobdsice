package pl.edu.agh.crrobdsice.entities.couchbase;


import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Document
public class Post implements Serializable {

    @Id
    private String id;

    @Field
    private Long ownerId;

    @Field
    private String content;

    @Field
    private Date creationDate;

    @Field
    private Date editionDate;

    @Field
    private Long likes;

    @Field
    private Set<String> tags;

    public Post() {
    }

    public Post(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public Post(String id, Long ownerId, String content) {
        this.id = id;
        this.ownerId = ownerId;
        this.content = content;
        this.creationDate = new Date();
        this.editionDate = new Date();
        this.likes = 0L;
        this.tags = new HashSet<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(Date editionDate) {
        this.editionDate = editionDate;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
}
