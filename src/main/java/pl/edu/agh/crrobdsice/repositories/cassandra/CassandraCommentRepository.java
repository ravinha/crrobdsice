package pl.edu.agh.crrobdsice.repositories.cassandra;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import pl.edu.agh.crrobdsice.entities.cassandra.Comment;
import pl.edu.agh.crrobdsice.entities.cassandra.CommentKey;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CassandraCommentRepository extends ReactiveCassandraRepository<Comment, CommentKey> {

    Flux<Comment> findAllByCommentKeyPostId(Long postId);

    Mono<Comment> findDistinctByCommentKeyPostIdAndCommentKeyCommentId(Long postId, Long commentId);

    @Query("UPDATE comments SET likes = likes+1 WHERE post_id=?0 AND comment_id=?1")
    Flux<Comment> likeByCommentKeyPostIdAndAndCommentKeyCommentId(Long postId, Long commentId);

    @Query("UPDATE comments SET content = ?2 WHERE post_id=?0 AND comment_id=?1")
    Mono<Comment> editByCommentKeyPostIdAndCommentKeyCommentId(Long postId, Long commentId, String content);

}
