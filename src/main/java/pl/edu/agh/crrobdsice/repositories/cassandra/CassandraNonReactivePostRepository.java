package pl.edu.agh.crrobdsice.repositories.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import pl.edu.agh.crrobdsice.entities.cassandra.Post;

import java.util.List;

public interface CassandraNonReactivePostRepository extends CassandraRepository<Post, Long> {

    @Query("UPDATE posts SET content = ?1 WHERE post_id=?0")
    Post editContent(Long postId, String content);

    @Query("UPDATE posts SET tags = tags + ?1 WHERE post_id=?0")
    Post tag(Long id, List<String> tag);

    @Query("Select * from posts where post_id = ?0 limit 1")
    Post findPostById(Long id);
}
