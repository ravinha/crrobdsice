package pl.edu.agh.crrobdsice.repositories.cassandra;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import pl.edu.agh.crrobdsice.entities.cassandra.Post;
import reactor.core.publisher.Mono;

import java.util.List;

public interface CassandraPostRepository extends ReactiveCassandraRepository<Post, Long> {

    @Query("UPDATE posts SET content = ?1 WHERE post_id=?0")
    Mono<Post> editContent(Long postId, String content);

    @Query("UPDATE posts SET tags = tags + ?1 WHERE post_id=?0")
    Mono<Post> tag(Long id, List<String> tag);

    @Query("Select * from posts where post_id = ?0 limit 1")
    Mono<Post> findPostById(Long id);
}
