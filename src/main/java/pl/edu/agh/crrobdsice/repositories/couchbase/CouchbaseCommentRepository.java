package pl.edu.agh.crrobdsice.repositories.couchbase;


import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import pl.edu.agh.crrobdsice.entities.couchbase.Comment;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CouchbaseCommentRepository extends ReactiveCouchbaseRepository<Comment, Long> {

    Flux<Comment> findAllByPostId(Long postId);

    Mono<Comment> findDistinctByPostIdAndCommentId(Long postId, Long commentId);

    @Query("UPDATE comments SET likes = likes+1 WHERE post_id=?0 AND comment_id=?1")
    Flux<Comment> likeByPostIdAndAndCommentId(Long postId, Long commentId);

    @Query("UPDATE comments SET content = ?2 WHERE post_id=?0 AND comment_id=?1")
    Mono<Comment> editByPostIdAndCommentId(Long postId, Long commentId, String content);
}
