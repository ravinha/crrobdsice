package pl.edu.agh.crrobdsice.repositories.couchbase;

import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import pl.edu.agh.crrobdsice.entities.couchbase.Post;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

@N1qlPrimaryIndexed
public interface CouchbasePostRepository extends ReactiveCouchbaseRepository<Post, String> {

    @Query("UPDATE crr USE KEYS $1 SET content = $2")
    Mono<Post> editContent(String postId, String content);

    @Query("UPDATE posts SET tags = tags + ?1 WHERE post_id=?0")
    Mono<Post> tag(Long id, Set<String> tags);

    @Query("#{#n1ql.selectEntity} limit 1")
    Mono<Post> findFirst1ByIdEquals(String id);

    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} limit 10")
    Flux<Post> findAll();
}
