package pl.edu.agh.crrobdsice.utils;

/**
 * Created by rafal on 06.01.18.
 */
public enum RestEnum {
    CB_POST_ADD,
    CB_POST_DEL,
    CB_POST_EDI,
    CB_POST_GET,
    CB_POST_TAG,
    CB_COMM_GET_ALL,
    CB_COMM_ADD,
    CB_COMM_GET_BY_ID,
    CB_COMM_EDI,
    CB_COMM_DEL,

    CA_POST_ADD,
    CA_POST_DEL,
    CA_POST_EDI,
    CA_POST_GET,
    CA_POST_TAG,
    CA_COMM_GET_ALL,
    CA_COMM_ADD,
    CA_COMM_GET_BY_ID,
    CA_COMM_EDI,
    CA_COMM_DEL;
}
