package pl.edu.agh.crrobdsice.utils;

import org.slf4j.LoggerFactory;
import pl.edu.agh.crrobdsice.entities.couchbase.Comment;
import pl.edu.agh.crrobdsice.entities.couchbase.Post;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by rafal on 06.01.18.
 */
public class StatsLogger {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger("stats");

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static final String SEPARATOR = ",";

    public static void logCbPost(RestEnum restEnum, Long startTime, Post e) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                Objects.nonNull(e) ? e.getOwnerId() : null,
                Objects.nonNull(e) ? e.getContent() : null,
                Objects.nonNull(e) ? Long.parseLong(e.getId()) : null,
                null
        );
    }

    public static void logCbComment(RestEnum restEnum, Long startTime, Comment e) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                Objects.nonNull(e) ? e.getOwnerId() : null,
                Objects.nonNull(e) ? e.getContent() : null,
                Objects.nonNull(e) ? e.getPostId() : null,
                Objects.nonNull(e) ? e.getCommentId() : null
        );
    }

    public static void logCbComment(RestEnum restEnum, Long startTime, Long commentId, Long postId) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                null,
                null,
                postId,
                commentId
        );
    }

    public static void logCbPost(RestEnum restEnum, Long startTime, Long postId) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                null,
                null,
                postId,
                null
        );
    }

    public static void logCaPostNonReactive(RestEnum restEnum, Long startTime, pl.edu.agh.crrobdsice.entities.cassandra.Post e) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                Objects.nonNull(e) ? e.getOwnerId() : null,
                Objects.nonNull(e) ? e.getContent() : null,
                Objects.nonNull(e) ? e.getId() : null,
                null
        );
    }

    public static void logCaPost(RestEnum restEnum, Long startTime, pl.edu.agh.crrobdsice.entities.cassandra.Post e) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                Objects.nonNull(e) ? e.getOwnerId() : null,
                Objects.nonNull(e) ? e.getContent() : null,
                Objects.nonNull(e) ? e.getId() : null,
                null
        );
    }

    public static void logCaComment(RestEnum restEnum, Long startTime, pl.edu.agh.crrobdsice.entities.cassandra.Comment e) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                Objects.nonNull(e) ? e.getOwnerId() : null,
                Objects.nonNull(e) ? e.getContent() : null,
                Objects.nonNull(e) ? e.getCommentKey().getPostId() : null,
                Objects.nonNull(e) ? e.getCommentKey().getCommentId() : null
        );
    }

    public static void logCaComment(RestEnum restEnum, Long startTime, Long commentId, Long postId) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                null,
                null,
                postId,
                commentId
        );
    }

    public static void logCaPost(RestEnum restEnum, Long startTime, Long postId) {
        StatsLogger.generateStatsLog(
                restEnum,
                System.currentTimeMillis() - startTime,
                null,
                null,
                postId,
                null
        );
    }

    public static void generateStatsLog(RestEnum restEnum,
                                        Long latency,
                                        Long userId,
                                        String content,
                                        Long postId,
                                        Long commentId) {
        LOGGER.debug(new StatsEntity(
                simpleDateFormat.format(System.currentTimeMillis()),
                restEnum.name(),
                StringUtils.emptyIfNull(latency),
                StringUtils.emptyIfNull(userId),
                StringUtils.withQuotes(content),
                StringUtils.emptyIfNull(postId),
                StringUtils.emptyIfNull(commentId)
        ).generateEvent(SEPARATOR));
    }

    private static class StatsEntity {

        private String timestamp;
        private String request;
        private String latency;
        private String userId;
        private String content;
        private String postId;
        private String commentId;
        private String error;

        public StatsEntity(String timestamp,
                           String request,
                           String latency,
                           String userId,
                           String content,
                           String postId,
                           String commentId) {
            this.timestamp = timestamp;
            this.request = request;
            this.latency = latency;
            this.userId = userId;
            this.content = content;
            this.postId = postId;
            this.commentId = commentId;
        }

        public String generateEvent(String separator) {
            return timestamp + separator +
                    request + separator +
                    latency + separator +
                    userId + separator +
                    content + separator +
                    postId + separator +
                    commentId;
        }
    }

}
