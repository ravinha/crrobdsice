package pl.edu.agh.crrobdsice.utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by rafal on 21.09.17.
 */
public class StringUtils {

    public static final String BLANK = "";

    public static String emptyIfNull(Object o) {
        if (Objects.nonNull(o)) {
            return o.toString();
        }
        return BLANK;
    }

    public static String nullStringIfNullObject(Object o) {
        if (Objects.nonNull(o)) {
            return o.toString();
        }
        return null;
    }


    public static String emptyIfNull(String s) {
        return Objects.isNull(s) ? BLANK : s;
    }

    public static String withQuotes(String stringParameter) {
        return "\"" + (Objects.isNull(stringParameter) ? BLANK : stringParameter) + "\"";
    }

    public static String dateFormat(Object value, DateFormat dateFormat) {
        if (Objects.nonNull(value) && value instanceof Date) {
            return dateFormat.format(value);
        }
        return BLANK;
    }

    public static Optional<String> castString(Object obj) {
        return Optional.ofNullable(Objects.nonNull(obj) && obj instanceof String ? (String) obj : null);
    }

    public static String removeSpecialCharacters(String rejectReason) {
        return rejectReason.replaceAll("[\n\t\r\" ]", " ");
    }
}
